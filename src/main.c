#include "mem.h"
#include <assert.h>
#include <malloc.h>

#define HEAP_SIZE 8175
#define BLOCK_SIZE 32

void success() {
    void *heap = heap_init(HEAP_SIZE);

    assert(heap != NULL);
    debug_heap(stdout, heap);

    void *block = _malloc(BLOCK_SIZE);
    assert(block != NULL);
    debug_heap(stdout, heap);

    _free(block);
    debug_heap(stdout, heap);
}

void free_one_block() {
    void *heap = heap_init(HEAP_SIZE);
    debug_heap(stdout, heap);

    void *block1 = _malloc(BLOCK_SIZE);
    void *block2 = _malloc(BLOCK_SIZE);
    void *block3 = _malloc(BLOCK_SIZE);
    void *block4 = _malloc(BLOCK_SIZE);
    debug_heap(stdout, heap);
    assert(block1 != NULL);
    assert(block2 != NULL);
    assert(block3 != NULL);
    assert(block4 != NULL);

    _free(block2);
    debug_heap(stdout, heap);
    _free(block1);
    _free(block3);
    _free(block4);
}

void free_two_blocks() {
    void *heap = heap_init(HEAP_SIZE);
    debug_heap(stdout, heap);

    void *block1 = _malloc(BLOCK_SIZE);
    void *block2 = _malloc(BLOCK_SIZE);
    void *block3 = _malloc(BLOCK_SIZE);
    void *block4 = _malloc(BLOCK_SIZE);
    debug_heap(stdout, heap);
    assert(block1 != NULL);
    assert(block2 != NULL);
    assert(block3 != NULL);
    assert(block4 != NULL);

    _free(block3);
    _free(block2);
    debug_heap(stdout, heap);

    _free(block1);
    _free(block4);
}

void expand_old_region() {
    void *heap = heap_init(HEAP_SIZE);
    debug_heap(stdout, heap);

    void *block1 = _malloc(8176);
    debug_heap(stdout, heap);
    assert(block1 != NULL);
    _free(block1);
}

void expand_old_region_v2() {
    void *heap = heap_init(HEAP_SIZE);
    debug_heap(stdout, heap);

    void *addr = mmap(HEAP_START, 10000, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, -1, 0);

    void *block1 = _malloc(HEAP_SIZE + 1);
    debug_heap(stdout, heap);
    assert(block1 != NULL);

    _free(block1);
    free(addr);
}

int main() {
    printf("Tests started.\n");
    printf("Test 1\n");
    success();
    printf("Test 1 success\n--------------------\nTest 2\n");
    free_one_block();
    printf("Test 2 success\n--------------------\nTest 3\n");
    free_two_blocks();
    printf("Test 3 success\n--------------------\nTest 4\n");
    expand_old_region();
    printf("Test 4 success\n--------------------\nTest 5\n");
    expand_old_region_v2();
    printf("Test 5 success\nAll tests passed.");
}